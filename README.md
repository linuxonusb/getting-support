
## How to get support for your "Linux on USB"


The product comes with a limited email support and a limited lifetime support via public forum.


### Email support

The primariy function of email support is to get you up and running.
If you have trouble running the system, the email support can be useful.
It's offerred for a limited time (say, 5-day or 7-day, etc. depending on the products),
and it's advised that you take advantage of this initial email support.

If you are new to Linux, this can be espeically helpful to you.


### Forum support

We provide a limited lifetime product support via a public forum.

The questions should be limited to specific product issues (since we won't be able to provide general support for broad Linux issues).


The first thing you'll need to do is to register in the forum.

We use gitlab for support platform. Gitlab is a developer platform, but it suits our needs.

first register on gitlab as a user,
then ask us for access to the "Linux Users" group.

When you have a quesiton, or need a help,
open an "issue" from the Issues page.

We'll try to reply to your questions/issues as soon as possible.
But, as a general rule, please allow 1~2 business days.






